# Sturmpistole

![](https://i.imgur.com/tzX42Pu.png)

An unofficial newgrounds API written in PHP

## Components

- Flask
- BeautifulSoup
- Selenium
- Chrome driver

## Running

### Windows
- Install all the required components in the `requirements.txt` via `pip install -r requirements.txt`
- Get the latest chrome driver from [Chromium](http://chromedriver.chromium.org/downloads)
- Extract the `chromedriver.exe` to the root folder of this project

**If you don't have virualenv installed:**
- Run `pip install virtualenv`

**If you do have virtualenv installed:**
- Import virualenv `venv` via `virtualenv venv`
- Run `.\venv\Scripts\activate.bat` for cmd or `.\venv\Scripts\activate.ps1` for PowerShell (On error with execution policy make sure that you are running PS via admin, if still complains - `set-executionpolicy remotesigned`).

You should be running in venv:
`(venv) PS C:\<repo location>\ng-scrapr>`

- Run `python app.py`

Flask should be app & running, keep an eye on the console window, tho ;)

### Linux
- Install all the required components in the `requirements.txt` via `pip3 install -r requirements.txt`
- Get the latest chrome driver from [Chromium](http://chromedriver.chromium.org/downloads)
- Extract the `chromedriver` to the root folder of this project

**If you don't have virualenv installed:**
- Run `pip install virtualenv`

**If you do have virtualenv installed:**
- Run `$ virtualenv venv --distribute`
- Run `$ source venv/bin/activate`

You should be running in venv:
`(venv) user@pc:/<repo location>/ng-scrapr$`

- Add chromium repo & install chrome

`wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -`

`echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list`

`sudo apt-get update && sudo apt-get install google-chrome-stable`
- Run `python3 app.py`

If you want to auto-download chrome webdriver from the web use `--chrome-auto` flag

## Coverage

### User
- [ ] Profile
- [ ] Fans
- [x] Movies
- [x] News
- [x] Art
- [x] Games
- [x] Audio
- [ ] Faves
- [ ] Reviews
- [ ] BBS posts
