# -*- coding: utf-8 -*-

from flask import *
import os, time, json, re, random, argparse, string
from string import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
# from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

os.environ["LANG"] = "en_US.UTF-8"
dirpath = os.getcwd()

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

parser = argparse.ArgumentParser(
    description='Unofficial newgrounds API',
)

parser.add_argument('--chrome-auto', action='store_true', help='Automatically download chrome webdriver')
args = parser.parse_args()


def init_driver():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--window-size=1920x1080")
    prefs = {"profile.managed_default_content_settings.images": 2, 'disk-cache-size': 4096}
    chrome_options.add_experimental_option('prefs', prefs)
    if os.name == 'nt':
        # driver = webdriver.Chrome(executable_path=os.getcwd() + '\\chromedriver.exe', options=chrome_options)
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    elif os.name == 'posix':
        chrome_options.add_argument("--no-sandbox")
        if args.chrome_auto:
            driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        else:
            driver = webdriver.Chrome(executable_path=os.getcwd() + '/chromedriver', options=chrome_options)
    driver.implicitly_wait(2)

    return driver


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico')


@app.route('/')
def index():
    return render_template("home.html", title='Home')


def driver_get_html(url):
    driver.get(url)
    html = driver.page_source
    soup = BeautifulSoup(html, 'lxml')
    return soup


@app.route('/api/v1.0/user/<username>/movies', methods=['GET'])
def get_movies(username):
    """Get movies
    Getting movies from NG
    """
    soup = driver_get_html("https://{}.newgrounds.com/movies".format(username))
    links = []
    """ Finding and parsing movie links """
    for a in soup.find_all('a', class_='item-portalsubmission-small'):
        """ For some reason ng inserts empty fucking spans of 1-100% width. I think it's rating in stars"""
        link_text = a.select('div.portalsubmission-icons a.item-portalsubmission-small .item-details span:first-child')
        """ So we remove it"""
        link_text.pop()
        """ Converting tags to text"""
        for l in link_text:
            link_text = l.text
        # print(link_text)
        links.append({"href": 'https:' + a['href'], "name": link_text})
        # print(link_text)

    driver.save_screenshot("{}_{}_screenshot.png".format(username, round(time.time())))
    # print(links)
    """ Returning json as output"""
    return jsonify(
        username=username,
        type="movies",
        links=links
    )


@app.route('/api/v1.0/user/<username>/games', methods=['GET'])
def get_games(username):
    """Get games
    Getting games from NG
    """
    soup = driver_get_html("https://{}.newgrounds.com/games".format(username))
    links = []
    """ Finding and parsing game links """
    for a in soup.find_all('a', class_='item-portalsubmission-small'):
        """ For some reason ng inserts empty fucking spans of 1-100% width. I think it's rating in stars"""
        link_text = a.select('div.portalsubmission-icons a.item-portalsubmission-small .item-details span:first-child')
        """ So we remove it"""
        link_text.pop()
        """ Converting tags to text"""
        for l in link_text:
            link_text = l.text
        # print(link_text)
        links.append({"href": 'https:' + a['href'], "name": link_text})
        # print(link_text)

    driver.save_screenshot("{}_{}_screenshot.png".format(username, round(time.time())))
    # print(links)
    """ Returning json as output"""
    return jsonify(
        username=username,
        type="games",
        links=links
    )


@app.route('/api/v1.0/user/<username>/art', methods=['GET'])
def get_art(username):
    """Get art
    Getting art from NG
    """
    soup = driver_get_html("https://{}.newgrounds.com/art".format(username))
    links = []
    """ Finding and parsing art links """
    for a in soup.find_all('a', class_='item-portalitem-art-small'):
        link_text = a.select('div.portalitem-art-icons a.item-portalitem-art-small .item-details')
        """ Converting tags to text"""
        for l in link_text:
            link_text = l.text
        # print(link_text)
        links.append({"href": 'https:' + a['href'], "name": link_text})
        # print(link_text)

    driver.save_screenshot("{}_{}_screenshot.png".format(username, round(time.time())))
    # print(links)
    """ Returning json as output"""
    return jsonify(
        username=username,
        type="art",
        links=links
    )


@app.route('/api/v1.0/user/<username>/audio', methods=['GET'])
def get_audio(username):
    """Get audio
    Getting audio from NG
    """
    soup = driver_get_html("https://{}.newgrounds.com/audio".format(username))
    links = []
    """ Finding and parsing audio links """
    for a in soup.find_all('a', class_='item-link'):
        link_text = a.select('div.item-audiosubmission-small a.item-link .detail-title')
        """ Converting tags to text"""
        for l in link_text:
            link_text = ''.join(l.text.split())
        # print(link_text)
        links.append({"href": 'https:' + a['href'], "name": link_text})
        # print(link_text)

    driver.save_screenshot("{}_{}_screenshot.png".format(username, round(time.time())))
    # print(links)
    """ Returning json as output"""
    return jsonify(
        username=username,
        type="art",
        links=links
    )


@app.route('/api/v1.0/user/<username>/news/<page>', methods=['GET'])
def get_news(username, page):
    """Get news
    Getting news from NG
    """
    soup = driver_get_html("https://{}.newgrounds.com/news/{}".format(username, page))
    links = []
    news_current_page = int(page)
    news_total_pages = int
    """ Finding and parsing news links """
    news_total_pages = soup.select('#outer-skin > div.body-main.userbody > div.body-center > div:first-child > div > div.column.wide > div:last-child > div > div > div > a:last-child > span')
    for n in news_total_pages:
        news_total_pages = ''.join(n.text.split())

    pods = soup.select('div.pod:not(:first-child):not(:last-child)')
    # print("Pod list incoming:")
    # print(pods)

    for item in pods:
        # print("Item list incoming:")
        # print(item)
        for a in item.select('div.pod-head > h2.noicon.emotes-small > a'):
            link_href = a['href']
            print("=== Link ===:")
            print(a['href'])
        link_text = item.select('div.pod-head > h2.noicon.emotes-small > a')
        """ Converting tags to text"""
        for l in link_text:
            regex = re.compile(r'[\n\r\t]')
            link_text = regex.sub(" ", l.text).strip()
        # print(link_text)
        links.append({"href": 'https:' + a['href'], "name": link_text})
        # print(link_text)

    driver.save_screenshot("{}_{}_screenshot.png".format(username, round(time.time())))
    # print(links)
    """ Returning json as output"""
    return jsonify(
        username=username,
        type="news",
        current_page=news_current_page,
        total_pages=int(news_total_pages),
        links=links
    )


if __name__ == '__main__':
    print("Initializing webdriver...")
    driver = init_driver()
    print("Launching Flask...")
    app.run(debug=True, use_reloader=False)
    app.add_url_rule('/favicon.ico', redirect_to=url_for('static', filename='favicon.ico'))
